<?php
require __DIR__ . '/__connect_db.php';

if(! isset($_SESSION['user'])) {
    echo 'no login';
    exit;
}
$amount = 0;
foreach($_SESSION['cart'] as $k => $v) {
    $amount += $v['price']*$v['qn'];
}


$rs = $mysqli->prepare("INSERT INTO `orders`
(`member_sid`, `amount`, `order_date`) VALUES
(?, ?, NOW() )");

$rs->bind_param('ii', $_SESSION['user']['sid'], $amount);
$rs->execute();

$sid = $rs->insert_id;
//echo "$sid <br>";
echo $mysqli->error;
$rs->close();

foreach($_SESSION['cart'] as $k => $v) {
    $sql = sprintf("INSERT INTO `order_details`
(`order_sid`, `product_sid`, `price`, `quantity`) VALUES
(%s, %s, %s, %s)",
        $sid,
        $k,
        $v['price'],
        $v['qn']
        );
    $mysqli->query($sql);
//    echo "$sql <br>";
}
//unset( $_SESSION['cart'] );
$_SESSION['cart'] = array();


echo 'success';

