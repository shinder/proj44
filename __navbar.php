<style>
    #cart-num {
        display: none;
    }
</style>
<nav class="navbar navbar-default navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./">Home</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <li class="<?= $pageName=='product_list' ? 'active' : '' ?>"><a href="product_list.php">產品列表</a></li>
                <li class="<?= $pageName=='cart_list' ? 'active' : '' ?>"><a href="cart_list.php">購物車 <span class="badge" id="cart-num">0</span></a></li>


            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if(isset($_SESSION['user'])): ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false"><?=$_SESSION['user']['nickname']?> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="logout.php">登出</a></li>
                            <li><a href="edit_user.php">修改個人資料</a></li>
                        </ul>
                    </li>
                <?php else: ?>
                    <li class=""><a href="login.php">登入</a></li>
                    <li class=""><a href="register.php">註冊</a></li>
                <?php endif; ?>


            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>