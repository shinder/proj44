CREATE TABLE `orders` (
  `sid` int(11) NOT NULL,
  `member_sid` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `order_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `order_details`
--

CREATE TABLE `order_details` (
  `sid` int(11) NOT NULL,
  `order_sid` int(11) NOT NULL,
  `product_sid` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `orders`
--
ALTER TABLE `orders`
ADD PRIMARY KEY (`sid`);

--
-- 資料表索引 `order_details`
--
ALTER TABLE `order_details`
ADD PRIMARY KEY (`sid`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `orders`
--
ALTER TABLE `orders`
MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `order_details`
--
ALTER TABLE `order_details`
MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT;
-- -------------------------------------------------------------------------

SELECT * FROM products JOIN categories
ON products.category_sid=categories.sid;

SELECT products.*, categories.name AS cate_name FROM products JOIN categories
ON products.category_sid=categories.sid;

SELECT products.*, categories.name cate_name FROM products JOIN categories
ON products.category_sid=categories.sid;

SELECT p.*, c.name cate_name FROM products p JOIN categories c
ON p.category_sid=c.sid;

SELECT p.*, c.name cate_name FROM products p LEFT JOIN categories c
ON p.category_sid=c.sid;

SELECT p.*, c.name cate_name FROM (
  SELECT * FROM products WHERE category_sid=2
) p LEFT JOIN categories c
ON p.category_sid=c.sid;

