<?php
    $pageName = 'data_list';
    require __DIR__ . '/__connect_db.php';

    $rs = $mysqli->query("SELECT sid FROM address_book");
    $totalRows = $rs->num_rows;

    $per_page = 5;
    $pageNum = isset($_GET['pageNum']) ? $_GET['pageNum'] : 1;
    $pageNum = max($pageNum, 1);
    $totalPages = ceil($totalRows/$per_page);

    $offset = ($pageNum-1)*$per_page;

    $rs2 = $mysqli->prepare("SELECT * FROM address_book ORDER BY sid DESC LIMIT ?, ? ");
    $rs2->bind_param('ii', $offset , $per_page);
    $rs2->execute();

    $rs2->bind_result($sid, $name, $phone, $email, $address, $birthday);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
    <style>
        .delete {
            color: red;
            font-size: 20px;
        }
    </style>
</head>
<body>

<div class="container">
    <?php
    include (__DIR__. '/__navbar.php');
    ?>

    <nav>
        <ul class="pagination">
            <li class=""><a href="?pageNum=1" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
            <?php for($i=-5; $i<=5; $i++):
                $p = $pageNum + $i;
                if($p > 0 and $p <= $totalPages):
                ?>
            <li class="<?= $i==0 ? 'active' : '' ?>"><a href="?pageNum=<?= $p ?>"><?= $p ?></a></li>
                <?php endif; ?>
            <?php endfor; ?>
            <li class=""><a href="?pageNum=<?= $totalPages ?>" aria-label="Next"><span aria-hidden="true">»</span></a></li>
        </ul>
    </nav>


    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>刪除</th>
            <th>編號</th>
            <th>姓名</th>
            <th>電話</th>
            <th>電郵</th>
            <th>地址</th>
            <th>生日</th>
            <th>修改</th>
        </tr>
        </thead>
        <tbody>
        <?php while ($rs2->fetch()):?>
        <tr>
            <td><a href="javascript:delete_it(<?= $sid ?>)">
                    <span class="glyphicon glyphicon-remove delete" aria-hidden="true"></span>
                </a>
            </td>
            <td><?= $sid ?></td>
            <td><?= $name ?></td>
            <td><?= $phone ?></td>
            <td><?= $email ?></td>
<!--            <td>--><?//= strip_tags($address) ?><!--</td>-->
            <td><?= htmlentities($address) ?></td>
            <td><?= $birthday ?></td>
            <td><a href="data_edit.php?sid=<?= $sid ?>">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                </a>
            </td>
        </tr>
        <?php endwhile; ?>
        </tbody>
    </table>
</div>


<script src="https://code.jquery.com/jquery-1.12.2.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script>
    function delete_it(sid) {
        if(confirm('確定要刪除編號為 '+sid+' 的資料嗎?')) {
            location.href = 'data_delete.php?sid='+sid;
        }
    }
</script>
</body>
</html>