$(function(){
    window.getCartNum = function(){
        $.get('add_to_cart.php', function(data){
            var total = 0;
            for(var s in data) {
                total += data[s]['qn'];
            }
            $('#cart-num').text(total);
            $('#cart-num').fadeIn();
        }, 'json');
    };
    window.getCartNum();
});