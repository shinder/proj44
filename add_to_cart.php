<?php
require __DIR__. '/__connect_db.php';

$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;
$qn = isset($_GET['qn']) ? intval($_GET['qn']) : 0;

if(! isset($_SESSION['cart'])) {
    $_SESSION['cart'] = array();
}

if(! empty($sid)) {

    if(empty($qn)) {
        unset($_SESSION['cart'][$sid]);
    } else {
        // $_SESSION['cart'][$sid] = $qn;
        if(isset($_SESSION['cart'][$sid])) {
            // 設定數量
            $_SESSION['cart'][$sid]['qn'] = $qn;
        } else {
            $rs = $mysqli->query("SELECT * FROM products WHERE sid= $sid");
            if($rs->num_rows) {
                $_SESSION['cart'][$sid] = $rs->fetch_assoc();
                $_SESSION['cart'][$sid]['qn'] = $qn;
            }
        }

    }

}



echo json_encode($_SESSION['cart'], JSON_UNESCAPED_UNICODE);

