<?php
$pageName = 'cart_list';

//include __DIR__ . '/__is_login.php';
require __DIR__ . '/__connect_db.php';


//$rs = $mysqli->query("SELECT sid FROM products");
//$totalRows = $rs->num_rows;
//
//$per_page = 8;
//$pageNum = isset($_GET['pageNum']) ? $_GET['pageNum'] : 1;
//$pageNum = max($pageNum, 1);
//$totalPages = ceil($totalRows/$per_page);
//
//$offset = ($pageNum-1)*$per_page;
//
//$rs2 = $mysqli->prepare("SELECT
//`sid`, `author`, `bookname`, `category_sid`,
//`book_id`, `publish_date`, `pages`, `price`,
//`introduction`
//
//FROM products ORDER BY sid DESC LIMIT ?, ? ");
//$rs2->bind_param('ii', $offset , $per_page);
//$rs2->execute();
//
//$rs2->bind_result(
//$sid, $author, $bookname, $category_sid,
//$book_id, $publish_date, $pages, $price,
//$introduction
//);

//echo $totalRows;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="colorbox/example1/colorbox.css">
    <style>
        .delete {
            color:red;
            font-size: 18px;
            cursor: pointer;
        }
    </style>
</head>
<body>

<div class="container">
    <?php
    include(__DIR__ . '/__navbar.php');
    ?>

    <div class="col-lg-12">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>取消</th>
                <th>封面</th>
                <th>書名</th>
                <th>作者</th>
                <th>價格</th>
                <th>數量</th>
                <th>小計</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($_SESSION['cart'] as $k=>$v ) :?>
            <tr data-sid="<?= $k ?>">
                <td>
                    <span class="glyphicon glyphicon-remove delete" aria-hidden="true"></span>
                </td>
                <td><img src="imgs/small/<?= $v['book_id'] ?>.jpg"></td>
                <td><?= $v['bookname'] ?></td>
                <td><?= $v['author'] ?></td>
                <td><?= $v['price'] ?></td>
                <td>
                    <select name="qn" class="qn">
                        <?php for($i=1; $i<=20; $i++): ?>
                            <option value="<?= $i ?>" <?= $i==$v['qn'] ? 'selected' : '' ?>><?= $i ?></option>.

                        <?php endfor; ?>
                    </select>
                </td>
                <td class="sub-total"><?= $v['qn']*$v['price'] ?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>


        <div class="col-lg-3 col-lg-offset-6">
            <div class="alert alert-info" role="alert">總計:<span class="badge" id="total-price">1200</span></div>
        </div>
        <div class="col-lg-3">
            <div class="btn btn-info pay-it"> 結帳 </div>
        </div>

    </div>


</div>


<script src="https://code.jquery.com/jquery-1.12.2.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="colorbox/jquery.colorbox-min.js"></script>
<script src="js/navbar.js"></script>
<script>
    $('.delete').click(function(){
        var sid = $(this).closest('tr').data('sid');
        var btn = $(this);

        $.get('add_to_cart.php', {sid:sid}, function(){
            btn.closest('tr').remove();
            window.getCartNum();
            calcTotal();
        });
    });

    $('.qn').change(function(){
        var sid = $(this).closest('tr').data('sid');
        var combo = $(this);
        var qn = parseInt( combo.val() );

        $.get('add_to_cart.php', {sid:sid, qn:qn}, function(){
            var $price_td = combo.parent().prev();
            var $sub_total_td = combo.parent().next();
            $sub_total_td.text( qn * $price_td.text() );
            window.getCartNum();
            calcTotal();
        });
    });

    $('.pay-it').click(function(){

        $.get('pay_it.php', function(data){
            if(data=='no login'){
                alert('請先登入再結帳');
            } else {
                alert('感謝購買');
            }
        });

    });

    var calcTotal = function(){
        var t = 0;
        $('.sub-total').each(function(){
            t+= parseInt( $(this).text() );
        });
        $('#total-price').text( t );
    };
    calcTotal();
</script>

</body>
</html>