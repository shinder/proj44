<?php

require __DIR__. '/__connect_db.php';

if( isset($_GET['sid']) ){
    $sid =  intval($_GET['sid']);
} else {
    die('No sid');
}

$rs = $mysqli->query("SELECT * FROM products WHERE sid= $sid ");

$row = $rs->fetch_assoc();
//$row = $rs->fetch_object();

//print_r($row);
//var_dump($row);
?>
<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title><?php echo $row['bookname']; ?></title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
</head>
<body>

<div class="container theme-showcase">
    <div class="col-lg-12">

        <div class="thumbnail" style="height:360px;">
            <img src="imgs/big/<?php echo $row['book_id']; ?>.png">
            <div class="caption">
                <h5><?php echo $row['bookname']; ?></h5>
                <h5><?php echo $row['author']; ?></h5>
                <p>
                    <span class="glyphicon glyphicon-search"></span>
                    <span class="label label-info">$ <?php echo $row['price']; ?></span>


                    <select name="qn" id="qn">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>

                    <button class="btn btn-warning btn-sm buy_btn" data-sid="<?= $row['sid']; ?>">買</button>
                </p>
            </div>
        </div>
    </div>


</div>
<script src="https://code.jquery.com/jquery-1.12.2.min.js"></script>
<script>
    $('.buy_btn').click(function(){
        var sid = $(this).data('sid');
        var qn = $('#qn').val();

        $.get('add_to_cart.php', {sid:sid, qn:qn}, function(data){
            alert('商品已加入購物車');

            window.parent.getCartNum();
        });
    });
</script>

</body>
</html>