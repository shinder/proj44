<?php
$pageName = 'register';


require __DIR__. '/__connect_db.php';

$email_id = '';
$nickname = '';
$mobile = '';
$address = '';


if(isset($_POST['email_id']) and isset($_POST['password'])) {

    $email_id = $_POST['email_id'];
    $password = sha1($_POST['password']);
    $nickname = $_POST['nickname'];

    $mobile = $_POST['mobile'];
    $address = $_POST['address'];
    $certification = md5( $email_id. uniqid() );

    $rs = $mysqli->prepare("INSERT INTO `members`
(`email_id`, `password`, `nickname`, `mobile`,
 `address`, `created_at`, `certification`)
  VALUES
   (?,?,?,?,
   ?, NOW(),?)");


    $rs->bind_param('ssssss', $email_id, $password, $nickname, $mobile, $address, $certification);
    $rs->execute();

    if($rs->insert_id){
        $_SESSION['flashMsg'] = array(
            'msg' => '註冊成功',
            'type' => 'success'
        );
//        header('Location: ./main.php');
//        exit;
    } else {
        $_SESSION['flashMsg'] = array(
            'msg' => '註冊失敗',
            'type' => 'danger'
        );
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>會員註冊</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
    <style>
        .alert-danger {
            display: none;
        }
    </style>
</head>
<body>

<div class="container">
    <?php
    include (__DIR__. '/__navbar.php');

    ?>
    <div class="col-lg-6">
        <?php if(isset($_SESSION['flashMsg'])):?>
            <div class="alert alert-<?= $_SESSION['flashMsg']['type'] ?>" role="alert"><?= $_SESSION['flashMsg']['msg'] ?></div>
        <?php endif; ?>
        <h2>會員註冊</h2>
        <form name="form1" class="form-horizontal" method="post" onsubmit="return formCheck();">
            <div class="form-group">
                <label for="email_id" class="col-sm-2 control-label">*email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="email_id" name="email_id" placeholder="email"
                    value="<?= $email_id ?>">
                    <div id="email_id_info" class="alert alert-danger" role="alert">請填寫正確的 email 格式</div>
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label">*密碼</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="password" name="password" placeholder="密碼">
                    <div id="password_info" class="alert alert-danger" role="alert">6位以</div>

                </div>
            </div>
            <div class="form-group">
                <label for="nickname" class="col-sm-2 control-label">*匿稱</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nickname" name="nickname" placeholder="匿稱"
                           value="<?= $nickname ?>">
                    <div id="nickname_info" class="alert alert-danger" role="alert">兩個字元以上</div>

                </div>
            </div>
            <div class="form-group">
                <label for="mobile" class="col-sm-2 control-label">手機</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="手機"
                           value="<?= $mobile ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="address" class="col-sm-2 control-label">地址</label>
                <div class="col-sm-10">
                    <textarea  id="address" name="address" class="form-control" rows="3"><?= $address ?></textarea>
                </div>
            </div>
            <?php if(!isset($_SESSION['flashMsg']) or $_SESSION['flashMsg']['type']!='success'): ?>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">註冊</button>
                </div>
            </div>
            <?php
            endif;
            unset($_SESSION['flashMsg']);
            ?>
        </form>
    </div>
</div>


<script src="https://code.jquery.com/jquery-1.12.2.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="js/navbar.js"></script>
<script>
    function formCheck(){
        var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var isPass = true;
        $('.alert-danger').hide();

        if(! pattern.test(form1.email_id.value)){
            $('#email_id_info').text('請填寫正確的 email 格式');
            $('#email_id_info').show();
            isPass = false;
        }

        if(form1.password.value.length<6){
            $('#password_info').show();
            isPass = false;

        }

        if(form1.nickname.value.length<2){
            $('#nickname_info').show();
            isPass = false;
        }

        if(isPass) {
            $.get('aj_email_id.php', {id: form1.email_id.value}, function(data){
                var x = parseInt(data);
                if(!x) {
                    form1.submit();
                } else {
                    $('#email_id_info').text('此 email 已被使用過');
                    $('#email_id_info').show();
                }
            });
        }

        return false;
    }


</script>

</body>
</html>