<?php
$pageName = 'product_list';
//include __DIR__ . '/__is_login.php';
require __DIR__. '/__connect_db.php';


$rs = $mysqli->query("SELECT sid FROM products");
$totalRows = $rs->num_rows;

$per_page = 8;
$pageNum = isset($_GET['pageNum']) ? $_GET['pageNum'] : 1;
$pageNum = max($pageNum, 1);
$totalPages = ceil($totalRows/$per_page);

$offset = ($pageNum-1)*$per_page;

$rs2 = $mysqli->prepare("SELECT
`sid`, `author`, `bookname`, `category_sid`,
`book_id`, `publish_date`, `pages`, `price`,
`introduction`

FROM products ORDER BY sid DESC LIMIT ?, ? ");
$rs2->bind_param('ii', $offset , $per_page);
$rs2->execute();

$rs2->bind_result(
$sid, $author, $bookname, $category_sid,
$book_id, $publish_date, $pages, $price,
$introduction
);

//echo $totalRows;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="colorbox/example1/colorbox.css">

</head>
<body>

<div class="container">
    <?php
    include (__DIR__. '/__navbar.php');
    ?>

    <div class="col-lg-3" col-sm-12>
        <div class="col-lg-12">
            <div class="btn-group-vertical col-lg-12">
                <button type="button" class="btn btn-info">1</button>
                <button type="button"  class="btn btn-info">2</button>

            </div>
        </div>
    </div>

    <div class="col-lg-9 col-sm-12">
        <div class="col-lg-12 col-sm-12">
            <nav>
                <ul class="pagination">
                    <li class=""><a href="?pageNum=1" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                    <?php for($i=-5; $i<=5; $i++):
                        $p = $pageNum + $i;
                        if($p > 0 and $p <= $totalPages):
                            ?>
                            <li class="<?= $i==0 ? 'active' : '' ?>"><a href="?pageNum=<?= $p ?>"><?= $p ?></a></li>
                        <?php endif; ?>
                    <?php endfor; ?>
                    <li class=""><a href="?pageNum=<?= $totalPages ?>" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                </ul>
            </nav>
        </div>


        <div class="col-lg-12 col-sm-12 col-xs-12">
            <?php  while($rs2->fetch()): ?>
            <div class="col-lg-3 col-sm-4 col-xs-6">
                <div class="thumbnail" style="height:280px; margin:10px 0;">
                    <a class="single_product" href="single_product.php?sid=<?= $sid ?>" target="_blank">
                        <img src="imgs/small/<?= $book_id ?>.jpg" style="width: 100px; height: 135px;">
                    </a>
                    <div class="caption">
                        <h5><?= $bookname ?></h5>
                        <h5><?= $author ?></h5>
                        <p>
                            <span class="glyphicon glyphicon-search"></span>
                            <span class="label label-info">$ <?= $price ?></span>
                            <select name="qn" class="qn">
                                <?php for($i=1; $i<=9; $i++): ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                                <?php endfor; ?>
                            </select>
                            <button class="btn btn-warning btn-sm buy_btn" data-sid="<?= $sid ?>">買</button>
                        </p>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>


        </div>

        <div class="col-lg-12">
            <nav>
                <ul class="pagination">
                    <li class=""><a href="?pageNum=1" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                    <?php for($i=-5; $i<=5; $i++):
                        $p = $pageNum + $i;
                        if($p > 0 and $p <= $totalPages):
                            ?>
                            <li class="<?= $i==0 ? 'active' : '' ?>"><a href="?pageNum=<?= $p ?>"><?= $p ?></a></li>
                        <?php endif; ?>
                    <?php endfor; ?>
                    <li class=""><a href="?pageNum=<?= $totalPages ?>" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                </ul>
            </nav>
        </div>



    </div>





</div>


<script src="https://code.jquery.com/jquery-1.12.2.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="colorbox/jquery.colorbox-min.js"></script>
<script src="js/navbar.js"></script>
<script>


    $(function(){
        $('.single_product').colorbox({iframe:true, innerWidth:500, innerHeight:409});

        $('.buy_btn').click(function(){
            // var qn = $(this).prev().val();
            var sid = $(this).data('sid');
            var qn = $(this).closest('.thumbnail').find('.qn').val();

            //alert($(this).data('sid') +':'+ qn );


            $.get('add_to_cart.php', {sid:sid, qn:qn}, function(data){
                alert('商品已加入購物車');
                window.getCartNum();
            });
        });
    });





</script>

</body>
</html>