<?php
$pageName = 'edit_user';


require __DIR__. '/__connect_db.php';

$email_id = '';
$nickname = '';
$mobile = '';
$address = '';


if(isset($_POST['email_id']) and isset($_POST['password'])) {

    $email_id = $_POST['email_id'];
    $password = sha1($_POST['password']);
    $new_password = sha1($_POST['new_password']);
    $nickname = $_POST['nickname'];

    $mobile = $_POST['mobile'];
    $address = $_POST['address'];

    $rs = $mysqli->prepare("SELECT sid FROM `members`
WHERE `email_id`=? AND `password`=?");

    $rs->bind_param('ss', $email_id, $password);
    $rs->execute();

    $rs->bind_result($sid);
    $rs->fetch();
    $rs->close();

    if($sid){
        if(empty( $_POST['new_password'] )){
            $rs2 = $mysqli->prepare("UPDATE `members` SET `nickname`=?, `mobile`=?, `address`=? WHERE sid= $sid");
            $rs2->bind_param('sss', $nickname, $mobile, $address);
        } else {
            $rs2 = $mysqli->prepare("UPDATE `members` SET `password`=?, `nickname`=?, `mobile`=?, `address`=? WHERE sid= $sid");
            $rs2->bind_param('ssssi', $new_password, $nickname, $mobile, $address);
        }

        if($rs2->execute()){
            $_SESSION['user'] = array(
                'email_id' => $email_id,
                'nickname' => $nickname,
                'mobile' => $mobile,
                'address' => $address,
            );

            $_SESSION['flashMsg'] = array(
                'msg' => '完成修改',
                'type' => 'success'
            );

        } else {

            $_SESSION['flashMsg'] = array(
                'msg' => '修改失敗',
                'type' => 'danger'
            );
        }
    } else {
        $_SESSION['flashMsg'] = array(
            'msg' => '密碼錯誤',
            'type' => 'danger'
        );
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>修改個人資料</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
    <style>
        .alert-danger {
            display: none;
        }
    </style>
</head>
<body>

<div class="container">
    <?php
    include (__DIR__. '/__navbar.php');

    ?>
    <div class="col-lg-6">
        <?php if(isset($_SESSION['flashMsg'])):?>
            <div class="alert alert-<?= $_SESSION['flashMsg']['type'] ?>" role="alert" style="display: block">
                <?= $_SESSION['flashMsg']['msg'] ?></div>
        <?php endif; ?>
        <h2>修改個人資料</h2>
        <form name="form1" class="form-horizontal" method="post" onsubmit="return formCheck();">
            <div class="form-group">
                <label for="email_id" class="col-sm-2 control-label">*email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="email_id" name="email_id" placeholder="email"
                    value="<?=$_SESSION['user']['email_id']?>" readonly="readonly">
                    <div id="email_id_info" class="alert alert-danger" role="alert">請填寫正確的 email 格式</div>
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label">*密碼</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="password" name="password" placeholder="密碼">
                    <div id="password_info" class="alert alert-danger" role="alert">6位以</div>

                </div>
            </div>
            <div class="form-group">
                <label for="new_password" class="col-sm-2 control-label">新密碼</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="new_password" name="new_password" placeholder="要修改才填入">
                    <div id="password_info" class="alert alert-danger" role="alert">4位以上</div>

                </div>
            </div>
            <div class="form-group">
                <label for="nickname" class="col-sm-2 control-label">匿稱</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nickname" name="nickname" placeholder="匿稱"
                           value="<?=$_SESSION['user']['nickname']?>">
                    <div id="nickname_info" class="alert alert-danger" role="alert">兩個字元以上</div>

                </div>
            </div>
            <div class="form-group">
                <label for="mobile" class="col-sm-2 control-label">手機</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="手機"
                           value="<?=$_SESSION['user']['mobile']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="address" class="col-sm-2 control-label">地址</label>
                <div class="col-sm-10">
                    <textarea  id="address" name="address" class="form-control" rows="3"><?=$_SESSION['user']['address']?></textarea>
                </div>
            </div>
            <?php if(!isset($_SESSION['flashMsg']) or $_SESSION['flashMsg']['type']!='success'): ?>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">修改</button>
                </div>
            </div>
            <?php
            endif;
            unset($_SESSION['flashMsg']);
            ?>
        </form>
    </div>
</div>


<script src="https://code.jquery.com/jquery-1.12.2.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="js/navbar.js"></script>
<script>
    function formCheck(){
        var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var isPass = true;
        $('.alert-danger').hide();


        if(form1.password.value.length<4){
            $('#password_info').show();
            isPass = false;

        }

        if(form1.nickname.value.length<2){
            $('#nickname_info').show();
            isPass = false;
        }


        return isPass;
    }


</script>

</body>
</html>